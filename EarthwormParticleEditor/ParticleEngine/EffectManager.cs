﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace EarthwormParticleEditor.ParticleEngine
{
    /// <summary>
    /// Class that manage all particle effect in scene
    /// </summary>
    class EffectManager
    {
        /// <summary>
        /// List of particle effect this class contain
        /// </summary>
        public List<ParticleEffect> ParticleEffectArray;
        private List<Particle> ParticleBucket;
        public Editor sceneManager;

        private uint particleIDrun;

        /// <summary>
        /// Create particle manager
        /// </summary>
        /// <param name="sm">SceneManager</param>
        public EffectManager(Editor sm)
        {
            sceneManager = sm;
            particleIDrun = 0;
        }
        /// <summary>
        /// Initialize particle manager, should be called in scene initialize method
        /// </summary>
        public void Init()
        {
            ParticleEffectArray = new List<ParticleEffect>();
            ParticleBucket = new List<Particle>();
        }
        /// <summary>
        /// Update particle manager and all particle effects in array, should be called in scene update method
        /// </summary>
        public void Update()
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                ParticleEffectArray[i].Update();
                //remove all unrepeat particle that run out
                if (!ParticleEffectArray[i].isRepeat)
                {
                    if (ParticleEffectArray[i].particleCreated >= ParticleEffectArray[i].MaximumParticle
                        && ParticleEffectArray[i].GetParticleAliveCount() <= 0
                        && !ParticleEffectArray[i].isRepeat)
                    {
                        ParticleEffectArray[i].Kill();
                        ParticleEffectArray.Remove(ParticleEffectArray[i]);
                        continue;
                    }
                    if (ParticleEffectArray[i].IsKilled()
                    && ParticleEffectArray[i].GetActiveParticle() <= 0)
                    {
                        ParticleEffectArray[i].Kill();
                        ParticleEffectArray.Remove(ParticleEffectArray[i]);
                        continue;
                    }
                }
            }
        }
        /// <summary>
        /// Draw all particle effects, should be called in scene draw method
        /// </summary>
        /// <param name="spriteBatch">SceneManager SpriteBatch</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                ParticleEffectArray[i].Draw(spriteBatch);
            }
        }
        /// <summary>
        /// Add new particle effect
        /// </summary>
        /// <param name="particle">Particle effect that prefer to added</param>
        public void Add(ParticleEffect particle)
        {
            ParticleEffectArray.Add(particle);
            particle.Load(sceneManager.GraphicsDevice);
            particle.ID = (int)particleIDrun;
            particleIDrun++;
        }
        /// <summary>
        /// Add new particle effect by template
        /// </summary>
        /// <param name="path">Template file path (if in Content folder, must be also add "Content\")</param>
        /// <param name="position">Vector 2D particle emit position</param>
        public void Add(string path, Vector3 position)
        {
            ParticleEffect particle = new ParticleEffect(path, this, sceneManager.Content);
            particle.EmitPosition = position;
            ParticleEffectArray.Add(particle);
            particle.Load(sceneManager.Content);
            particle.ID = (int)particleIDrun;
            particleIDrun++;
        }
        /// <summary>
        /// Seek particle effect in array by ID
        /// </summary>
        /// <param name="ID">Particle effect ID</param>
        /// <returns>Particle effect (return null if not found)</returns>
        public ParticleEffect Get(int ID)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                if (ParticleEffectArray[i].ID == ID)
                    return ParticleEffectArray[i];
            }
            return null;
        }
        /// <summary>
        /// Remove particle effect from array
        /// </summary>
        /// <param name="ID">Particle effect ID</param>
        public void Remove(int ID)
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                if (ParticleEffectArray[i].ID == ID)
                {
                    ParticleEffectArray[i].RemoveAllParticle();
                    ParticleEffectArray.Remove(ParticleEffectArray[i]);
                }
            }
        }

        /// <summary>
        /// Remove all particle effects in array
        /// </summary>
        public void RemoveAll()
        {
            for (int i = 0; i < ParticleEffectArray.Count; i++)
            {
                ParticleEffectArray[i].RemoveAllParticle();
                ParticleEffectArray[i].Kill();
                ParticleEffectArray.Remove(ParticleEffectArray[i]);
                ParticleBucket = new List<Particle>();
                ParticleEffectArray = new List<ParticleEffect>();
            }
        }

        /// <summary>
        /// Get an inactive particle
        /// </summary>
        /// <returns>an inactive particle</returns>
        public Particle GetInactiveParticle()
        {
            Particle par = null;
            for (int i = 0; i < ParticleBucket.Count; i++)
            {
                if (!ParticleBucket[i].Active)
                {
                    par = ParticleBucket[i];
                    break;
                }
            }
            if (par == null)
            {
                par = new Particle();
                ParticleBucket.Add(par);
            }
            return par;
        }
    }
}
