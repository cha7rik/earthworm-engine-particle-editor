﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace EarthwormParticleEditor.ParticleEngine
{
    /// <summary>
    /// Class handle all input form keyboard
    /// </summary>
    static class KeyboardInput
    {
        private static KeyboardState keyState;
        private static KeyboardState oldState;

        /// <summary>
        /// Update all keyboard input status
        /// Need to call after scene update
        /// </summary>
        public static void Update() //Need to call after scene update
        {
            oldState = Keyboard.GetState();
        }

        /// <summary>
        /// Check if key pressed
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is pressed</returns>
        public static bool KeyDown(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if key not pressed
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key not pressed</returns>
        public static bool KeyUp(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if Key was just hit
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key hit</returns>
        public static bool KeyHit(Keys key)
        {
            keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(key) && oldState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Check if Key was just release
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>Is key release</returns>
        public static bool KeyRelease(Keys key)
        {
            keyState = Keyboard.GetState();
            if (oldState.IsKeyDown(key) && keyState.IsKeyUp(key))
                return true;
            return false;
        }
        /// <summary>
        /// Get multiple keys pressed
        /// </summary>
        /// <returns>List of pressed keys</returns>
        public static List<Keys> GetKeyPress()
        {
            List<Keys> res = null;
            keyState = Keyboard.GetState();
            res = new List<Keys>(keyState.GetPressedKeys());
            return res;
        }
    }
}
