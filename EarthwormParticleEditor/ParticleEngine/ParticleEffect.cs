﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.IO;

namespace EarthwormParticleEditor.ParticleEngine
{
    /// <summary>
    /// Particle Effect class, create and manage all particle
    /// </summary>
    class ParticleEffect
    {
        public int ID;
        private Random rand;
        public Vector3 EmitPosition;
        public Vector3 EmitPositionRange;
        public uint MaximumParticle;
        public float ParticleGenerateTime;
        public float ParticleGenerateTimeRange;
        public bool isRepeat;
        private Texture2D texture;
        private List<Particle> particleArray;
        private string textureName;
        private float ParticleGenerateTimeCount;
        public int particleCreated;
        private bool isKilled;

        //particle properties
        private Vector3 Gravity;
        private Vector3 Direction;
        private Vector3 DirectionRange;
        private float SpeedRange;
        private float Angle;
        private float AngularVelocity;
        private float AngularVelocityRange;
        private Vector4 StartColor;
        private Vector4 FinalColor;
        private float Size;
        private float MaxSize;
        private float TTL;
        private float TTLRange;
        private float Depth;
        private Vector2 SpriteCut;
        private int AnimateTime;
        private bool isRandomFrame;

        private EffectManager particleManager;
        /// <summary>
        /// Create uninitialize particle effect
        /// </summary>
        public ParticleEffect() { }
        /// <summary>
        /// Create initialize particle effect
        /// </summary>
        /// <param name="pm">Particle Manager that contain this particle effect</param>
        /// <param name="position">Emit position</param>
        /// <param name="positionRange">Emit position range</param>
        /// <param name="maxParticle">Max particle count for this effect</param>
        /// <param name="particleGenerateTime">Particle generate time for this effect as secound</param>
        /// <param name="particleGenerateTimeRange">Particle generate time range for this effect as secound</param>
        /// <param name="particleTextures">Particle texture</param>
        /// <param name="cm">SceneManager ContentManager</param>
        /// <param name="gravity">Particle gravity</param>
        /// <param name="velocity">Particle move speed</param>
        /// <param name="directionRange">Particle direction range</param>
        /// <param name="speedRange">Particle speed range</param>
        /// <param name="angle">Particle rotation angle</param>
        /// <param name="angularVelocity">Particle rotation speed</param>
        /// <param name="angularVelocityRange">Particle rotation speed range</param>
        /// <param name="color">Particle color at beginnig</param>
        /// <param name="finalColor">Particle color at end</param>
        /// <param name="size">Particle size at beginning</param>
        /// <param name="maxSize">Particle size at end</param>
        /// <param name="ttl">Particle time to live</param>
        /// <param name="ttlRange">Particle time to live range</param>
        /// <param name="depth">Particle layer deptj</param>
        /// <param name="repeat">Is particle repeat endlessly</param>
        /// <param name="randomFrame">Is pick random frame</param>
        public ParticleEffect(EffectManager pm, Vector3 position, Vector3 positionRange, uint maxParticle, float particleGenerateTime, float particleGenerateTimeRange, string particleTextures, ContentManager cm,
            Vector3 gravity, Vector3 velocity, Vector3 directionRange, float speedRange, float angle, float angularVelocity, float angularVelocityRange, Vector4 color, Vector4 finalColor, float size, float maxSize, float ttl, float ttlRange, float depth,
            bool repeat, bool randomFrame)
        {
            ID = -1;
            rand = new Random();
            EmitPosition = position;
            EmitPositionRange = positionRange;
            MaximumParticle = maxParticle;
            ParticleGenerateTime = particleGenerateTime;
            ParticleGenerateTimeRange = particleGenerateTimeRange;
            ParticleGenerateTimeCount = particleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange)*-1,Math.Abs(ParticleGenerateTimeRange));
            textureName = particleTextures;
            isRepeat = repeat;
            particleCreated = 0;
            DirectionRange = directionRange;
            SpeedRange = speedRange;

            this.Load(cm);
            particleArray = new List<Particle>((int)maxParticle);

            isKilled = false;

            //set particle properties
            Gravity = gravity;
            Direction = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            AngularVelocityRange = angularVelocityRange;
            StartColor = color;
            FinalColor = finalColor;
            Size = size;
            MaxSize = maxSize;
            TTL = ttl;
            TTLRange = ttlRange;
            Depth = depth;
            SpriteCut = new Vector2(texture.Width, texture.Height);
            AnimateTime = 0;
            isRandomFrame = randomFrame;

            particleManager = pm;
        }

        /// <summary>
        /// Create initialize particle effect and animate
        /// </summary>
        /// <param name="pm">Particle Manager that contain this particle effect</param>
        /// <param name="position">Emit position</param>
        /// <param name="positionRange">Emit position range</param>
        /// <param name="maxParticle">Max particle count for this effect</param>
        /// <param name="particleGenerateTime">Particle generate time for this effect as secound</param>
        /// <param name="particleGenerateTimeRange">Particle generate time range for this effect as secound</param>
        /// <param name="particleTextures">Particle texture</param>
        /// <param name="cm">SceneManager ContentManager</param>
        /// <param name="gravity">Particle gravity</param>
        /// <param name="velocity">Particle move speed</param>
        /// <param name="directionRange">Particle direction range</param>
        /// <param name="speedRange">Particle speed range</param>
        /// <param name="angle">Particle rotation angle</param>
        /// <param name="angularVelocity">Particle rotation speed</param>
        /// <param name="angularVelocityRange">Particle rotation speed range</param>
        /// <param name="color">Particle color at beginnig</param>
        /// <param name="finalColor">Particle color at end</param>
        /// <param name="size">Particle size at beginning</param>
        /// <param name="maxSize">Particle size at end</param>
        /// <param name="ttlMin">Particle time to live</param>
        /// <param name="ttlMax">Particle time to live range</param>
        /// <param name="depth">Particle layer deptj</param>
        /// <param name="repeat">Is particle repeat endlessly</param>
        /// <param name="spriteCut">Sprite frame size</param>
        /// <param name="animateTime">Animate time by frame</param>
        /// <param name="randomFrame">Is pick random frame</param>
        public ParticleEffect(EffectManager pm, Vector3 position, Vector3 positionRange, uint maxParticle, float particleGenerateTime,float particleGenerateTimeRange, string particleTextures, ContentManager cm,
            Vector3 gravity, Vector3 velocity, Vector3 directionRange, float speedRange, float angle, float angularVelocity, float angularVelocityRange, Vector4 color, Vector4 finalColor, float size, float maxSize, float ttl, float ttlRange, float depth,
            bool repeat, Vector2 spriteCut, int animateTime, bool randomFrame)
        {
            ID = -1;
            rand = new Random();
            EmitPosition = position;
            EmitPositionRange = positionRange;
            MaximumParticle = maxParticle;
            ParticleGenerateTime = particleGenerateTime;
            ParticleGenerateTimeRange = particleGenerateTimeRange;
            ParticleGenerateTimeCount = particleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange) * -1, Math.Abs(ParticleGenerateTimeRange));
            textureName = particleTextures;
            isRepeat = repeat;
            particleCreated = 0;
            DirectionRange = directionRange;
            SpeedRange = speedRange;

            particleArray = new List<Particle>((int)maxParticle);

            isKilled = false;

            //set particle properties
            Gravity = gravity;
            Direction = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            AngularVelocityRange = angularVelocityRange;
            StartColor = color;
            FinalColor = finalColor;
            Size = size;
            MaxSize = maxSize;
            TTL = ttl;
            TTLRange = ttlRange;
            Depth = depth;
            SpriteCut = spriteCut;
            AnimateTime = animateTime;

            particleManager = pm;
        }

        public ParticleEffect(GraphicsDevice gd, EffectManager pm, Vector3 position, Vector3 positionRange, uint maxParticle, float particleGenerateTime,float particleGenerateTimeRange, string particleTextures, ContentManager cm,
            Vector3 gravity, Vector3 velocity, Vector3 directionRange, float speedRange, float angle, float angularVelocity, float angularVelocityRange, Vector4 color, Vector4 finalColor, float size, float maxSize, float ttl, float ttlRange, float depth,
            bool repeat, Vector2 spriteCut, int animateTime, bool randomFrame)
        {
            ID = -1;
            rand = new Random();
            EmitPosition = position;
            EmitPositionRange = positionRange;
            MaximumParticle = maxParticle;
            ParticleGenerateTime = particleGenerateTime;
            ParticleGenerateTimeRange = particleGenerateTimeRange;
            ParticleGenerateTimeCount = particleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange) * -1, Math.Abs(ParticleGenerateTimeRange));
            textureName = particleTextures;
            isRepeat = repeat;
            particleCreated = 0;
            DirectionRange = directionRange;
            SpeedRange = speedRange;

            particleArray = new List<Particle>((int)maxParticle);

            isKilled = false;

            //set particle properties
            Gravity = gravity;
            Direction = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            AngularVelocityRange = angularVelocityRange;
            StartColor = color;
            FinalColor = finalColor;
            Size = size;
            MaxSize = maxSize;
            TTL = ttl;
            TTLRange = ttlRange;
            Depth = depth;
            SpriteCut = spriteCut;
            AnimateTime = animateTime;
            isRandomFrame = randomFrame;

            particleManager = pm;
        }

        /// <summary>
        /// Create particle by load template form file
        /// </summary>
        /// <param name="path">Preset file path</param>
        /// <param name="pm">Particle Manager</param>
        /// <param name="cm">SceneManager ContentManager</param>
        public ParticleEffect(string path, EffectManager pm, ContentManager cm)
        {
            //Load properties form template
            ID = -1;
            rand = new Random();
            //Load properties form template
            this.LoadTemplate(path);
            particleManager = pm;
            isKilled = false;
            particleArray = new List<Particle>((int)MaximumParticle);
        }
        /// <summary>
        /// Load particle content
        /// </summary>
        /// <param name="contentManager">SceneManager ContentManager</param>
        public void Load(ContentManager contentManager)
        {
            texture = contentManager.Load<Texture2D>(textureName);
        }

        public void Load(GraphicsDevice gd)
        {
            using (FileStream titleStream = new FileStream(textureName, FileMode.Open))
            {
                texture = Texture2D.FromStream(gd, titleStream);
                titleStream.Close();

                Color[] buffer = new Color[texture.Width * texture.Height];
                texture.GetData(buffer);
                for (int i = 0; i < buffer.Length; i++)
                    buffer[i] = Color.FromNonPremultiplied(buffer[i].R, buffer[i].G, buffer[i].B, buffer[i].A);
                texture.SetData(buffer);
            }
        }

        /// <summary>
        /// Update all particle
        /// </summary>
        public void Update()
        {
            //create particle
            if (this.GetActiveParticle() < MaximumParticle
                && particleCreated < MaximumParticle
                && !isKilled)
            {
                if (ParticleGenerateTime == 0)
                {
                    for (int i = 0; i < MaximumParticle; i++)
                    {
                        Particle par = particleManager.GetInactiveParticle();
                        par.Activate(texture,
                                new Vector3(EmitPosition.X + rand.Next((int)EmitPositionRange.X / -2, (int)EmitPositionRange.X / 2), EmitPosition.Y + rand.Next((int)EmitPositionRange.Y / -2, (int)EmitPositionRange.Y / 2), EmitPosition.Y + rand.Next((int)EmitPositionRange.Z / -2, (int)EmitPositionRange.Z / 2)),
                                Gravity,
                                Vector3.Normalize(Direction + new Vector3(Calculator.RandomFloat(DirectionRange.X / -2, DirectionRange.X / 2), Calculator.RandomFloat(DirectionRange.Y / -2, DirectionRange.Y / 2), Calculator.RandomFloat(DirectionRange.Z / -2, DirectionRange.Z / 2))) * (Direction.Length() + Calculator.RandomFloat(SpeedRange / -2, SpeedRange / 2)),
                                Angle,
                                AngularVelocity,
                                StartColor,
                                FinalColor,
                                Size,
                                MaxSize,
                                TTL + Calculator.RandomFloat(TTLRange * -1, TTLRange),
                                Depth,
                                SpriteCut,
                                AnimateTime);
                        //set random frame
                        if (isRandomFrame)
                            par.SpriteAnimation.SetFrame(rand.Next(0, par.SpriteAnimation.GetTotalFrame() - 1));

                        particleArray.Add(par);

                        if (!isRepeat)
                            particleCreated++;

                        ParticleGenerateTimeCount = ParticleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange) * -1, Math.Abs(ParticleGenerateTimeRange));
                    }
                }
                else if (ParticleGenerateTimeCount > 0)
                    ParticleGenerateTimeCount -= Calculator.UnitsPerSecound(1.0f);
                else
                {
                    if (particleCreated < MaximumParticle)
                    {
                        Particle par = particleManager.GetInactiveParticle();
                        par.Activate(texture,
                                new Vector3(EmitPosition.X + rand.Next((int)EmitPositionRange.X / -2, (int)EmitPositionRange.X / 2), EmitPosition.Y + rand.Next((int)EmitPositionRange.Y / -2, (int)EmitPositionRange.Y / 2), EmitPosition.Z + rand.Next((int)EmitPositionRange.Z / -2, (int)EmitPositionRange.Z / 2)),
                                Gravity,
                                Vector3.Normalize(Direction + new Vector3(Calculator.RandomFloat(DirectionRange.X / -2, DirectionRange.X / 2), Calculator.RandomFloat(DirectionRange.Y / -2, DirectionRange.Y / 2), Calculator.RandomFloat(DirectionRange.Z / -2, DirectionRange.Z / 2))) * (Direction.Length() + Calculator.RandomFloat(SpeedRange / -2, SpeedRange / 2)),
                                Angle,
                                AngularVelocity + Calculator.RandomFloat(Math.Abs(AngularVelocityRange)*-1,Math.Abs(AngularVelocityRange)),
                                StartColor,
                                FinalColor,
                                Size,
                                MaxSize,
                                TTL + Calculator.RandomFloat(Math.Abs(TTLRange) * -1, Math.Abs(TTLRange)),
                                Depth,
                                SpriteCut,
                                AnimateTime);
                        //set random frame
                        if (isRandomFrame)
                            par.SpriteAnimation.SetFrame(rand.Next(0, par.SpriteAnimation.GetTotalFrame() - 1));

                        particleArray.Add(par);

                        if (!isRepeat)
                            particleCreated++;

                        ParticleGenerateTimeCount = ParticleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange) * -1, Math.Abs(ParticleGenerateTimeRange));
                    }
                }
            }

            //set as killed particle effect if out for particle and not repeat
            if (particleCreated >= MaximumParticle
                && GetParticleAliveCount() <= 0
                && !isRepeat)
            {
                this.Kill();
            }

            //update particle
            for (int i = 0; i < particleArray.Count; i++)
            {
                particleArray[i].Update();
                if (particleArray[i].TTL <= 0)
                {
                    particleArray[i].Active = false;
                    particleArray.Remove(particleArray[i]);
                }
            }
        }
        /// <summary>
        /// Draw all particle sprite
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < particleArray.Count; i++)
            {
                particleArray[i].Draw(spriteBatch);
            }
        }
        /// <summary>
        /// Get alive particle count in array
        /// </summary>
        /// <returns>Alive partice count</returns>
        public int GetParticleAliveCount()
        {
            return particleArray.Count;
        }
        /// <summary>
        /// Load particle effect template
        /// </summary>
        /// <param name="path">Template flie path</param>
        public void LoadTemplate(string path)
        {
            StreamReader sr = new StreamReader(path);
            string[] load = sr.ReadLine().Split('=');
            sr.Close();

            string emitpos = load[0];
            MaximumParticle = uint.Parse(load[1]);
            ParticleGenerateTime = float.Parse(load[2]);
            ParticleGenerateTimeRange = float.Parse(load[3]);
            ParticleGenerateTimeCount = ParticleGenerateTime + Calculator.RandomFloat(Math.Abs(ParticleGenerateTimeRange) * -1, Math.Abs(ParticleGenerateTimeRange));
            string gra = load[4];
            string dir = load[5];
            string dirRange = load[6];
            SpeedRange = float.Parse(load[7]);
            Angle = float.Parse(load[8]);
            AngularVelocity = float.Parse(load[9]);
            AngularVelocityRange = float.Parse(load[10]);
            string sColor = load[11];
            string fColor = load[12];
            Size = float.Parse(load[13]);
            MaxSize = float.Parse(load[14]);
            TTL = float.Parse(load[15]);
            TTLRange = float.Parse(load[16]);
            Depth = float.Parse(load[17]);
            isRepeat = bool.Parse(load[18]);
            string sCut = load[19];
            AnimateTime = int.Parse(load[20]);
            //21 is for editor load
            textureName = "resources/particles/" + load[22];
            isRandomFrame = bool.Parse(load[23]);

            load = emitpos.Split(',');
            EmitPositionRange.X = float.Parse(load[0]);
            EmitPositionRange.Y = float.Parse(load[1]);
            EmitPositionRange.Z = float.Parse(load[2]);
            load = gra.Split(',');
            Gravity.X = float.Parse(load[0]);
            Gravity.Y = float.Parse(load[1]);
            Gravity.Z = float.Parse(load[2]);
            load = dir.Split(',');
            Direction.X = float.Parse(load[0]);
            Direction.Y = float.Parse(load[1]);
            Direction.Z = float.Parse(load[2]);
            load = dirRange.Split(',');
            DirectionRange.X = float.Parse(load[0]);
            DirectionRange.Y = float.Parse(load[1]);
            DirectionRange.Z = float.Parse(load[2]);
            load = sColor.Split(',');
            StartColor = new Vector4(float.Parse(load[0]), float.Parse(load[1]), float.Parse(load[2]), float.Parse(load[3]));
            load = fColor.Split(',');
            FinalColor = new Vector4(float.Parse(load[0]), float.Parse(load[1]), float.Parse(load[2]), float.Parse(load[3]));
            load = sCut.Split(',');
            SpriteCut.X = float.Parse(load[0]);
            SpriteCut.Y = float.Parse(load[1]);
        }
        /// <summary>
        /// Kill particle effect
        /// </summary>
        public void Kill()
        {
            isKilled = true;
            texture.Dispose();
        }
        /// <summary>
        /// Get is particle killed
        /// </summary>
        /// <returns>Is particle killed</returns>
        public bool IsKilled()
        {
            return isKilled;
        }
        /// <summary>
        /// Remove all particle in array
        /// </summary>
        public void RemoveAllParticle()
        {
            for (int i = 0; i < particleArray.Count; i++)
                particleArray[i].Active = false;
        }
        /// <summary>
        /// Get active particle count
        /// </summary>
        /// <returns>Particle count</returns>
        public int GetActiveParticle()
        {
            int r = 0;
            for (int i = 0; i < particleArray.Count; i++)
            {
                if (particleArray[i].Active)
                    r++;
            }
            return r;
        }
    }
}
