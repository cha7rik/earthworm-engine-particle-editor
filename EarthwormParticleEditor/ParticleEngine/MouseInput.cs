﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace EarthwormParticleEditor.ParticleEngine
{
    /// <summary>
    /// Class handle all input form mouse
    /// </summary>
    static class MouseInput
    {
        private static MouseState mInput;
        private static MouseState oldInput;
        private static Vector2 cursorPos = new Vector2(0,0);

        /// <summary>
        /// Update mouse input status, should be called in scene update method
        /// Need to call after scene update
        /// </summary>
        public static void Update() //Need to call after scene update
        {
            oldInput = Mouse.GetState();
            
        }

        /// <summary>
        /// Check if mouse button pressed
        /// </summary>
        /// <param name="mButton">Mouse boutton to check</param>
        /// <returns>Is mouse button pressed</returns>
        public static bool ButtonDown(MouseButton mButton)
        {
            mInput = Mouse.GetState();
            switch(mButton)
            {
                case MouseButton.MouseButtonLeft :
                    if (mInput.LeftButton == ButtonState.Pressed)
                        return true;
                        break;
                case MouseButton.MouseButtonMiddle :
                    if (mInput.MiddleButton == ButtonState.Pressed)
                        return true;
                        break;
                case MouseButton.MouseButtonRight:
                        if (mInput.RightButton == ButtonState.Pressed)
                            return true;
                        break;
            }
            return false;     
        }
        /// <summary>
        /// Check if mouse button not pressed
        /// </summary>
        /// <param name="mButton">Mouse boutton to check</param>
        /// <returns>Is mouse button not pressed</returns>
        public static bool ButtonUp(MouseButton mButton)
        {
            mInput = Mouse.GetState();
            switch (mButton)
            {
                case MouseButton.MouseButtonLeft:
                    if (mInput.LeftButton == ButtonState.Released)
                        return true;
                    break;
                case MouseButton.MouseButtonMiddle:
                    if (mInput.MiddleButton == ButtonState.Released)
                        return true;
                    break;
                case MouseButton.MouseButtonRight:
                    if (mInput.RightButton == ButtonState.Released)
                        return true;
                    break;
            }
            return false;
        }
        /// <summary>
        /// Check if mouse button was just hit
        /// </summary>
        /// <param name="mButton">Mouse button to check</param>
        /// <returns>Is mouse button hit</returns>
        public static bool ButtonHit(MouseButton mButton)
        {
            mInput = Mouse.GetState();
            switch (mButton)
            {
                case MouseButton.MouseButtonLeft:
                    if (mInput.LeftButton == ButtonState.Pressed && oldInput.LeftButton == ButtonState.Released)
                        return true;
                    break;
                case MouseButton.MouseButtonMiddle:
                    if (mInput.MiddleButton == ButtonState.Pressed && oldInput.MiddleButton == ButtonState.Released)
                        return true;
                    break;
                case MouseButton.MouseButtonRight:
                    if (mInput.RightButton == ButtonState.Released && oldInput.RightButton == ButtonState.Released)
                        return true;
                    break;
            }
            return false;
        }
        /// <summary>
        /// Check if mouse button was just release
        /// </summary>
        /// <param name="mButton">Mouse button to check</param>
        /// <returns>Is mouse button release</returns>
        public static bool ButtonRelease(MouseButton mButton)
        {
            mInput = Mouse.GetState();
            switch (mButton)
            {
                case MouseButton.MouseButtonLeft:
                    if (mInput.LeftButton == ButtonState.Released && oldInput.LeftButton == ButtonState.Pressed)
                        return true;
                    break;
                case MouseButton.MouseButtonMiddle:
                    if (mInput.MiddleButton == ButtonState.Released && oldInput.MiddleButton == ButtonState.Pressed)
                        return true;
                    break;
                case MouseButton.MouseButtonRight:
                    if (mInput.RightButton == ButtonState.Released && oldInput.RightButton == ButtonState.Pressed)
                        return true;
                    break;
            }
            return false;
        }
        /// <summary>
        /// Get mouse cursor position relate to game screen
        /// </summary>
        /// <returns>Vector 2D cursor position</returns>
        public static Vector2 GetCursorPosition()
        {
            mInput = Mouse.GetState();
            cursorPos.X = mInput.X;
            cursorPos.Y = mInput.Y;
            return cursorPos; 
        }
        /*
        /// <summary>
        /// Get mouse cursor position relate to game world
        /// </summary>
        /// <param name="sceneCamera">Scene camera object</param>
        /// <returns>Vector 2D cursor position</returns>
        public static Vector2 GetCursorPosition(Camera sceneCamera)
        {
            mInput = Mouse.GetState();
            cursorPos.X = mInput.X;
            cursorPos.Y = mInput.Y;
            return Vector2.Transform(cursorPos, Matrix.Invert(sceneCamera.GetCameraTranformation())) - (ResolutionManager.getMarginSize() / sceneCamera.GetScale());
        }
        */
    }

    /// <summary>
    /// Mouse Button
    /// </summary>
    enum MouseButton
    {
        MouseButtonLeft,
        MouseButtonMiddle,
        MouseButtonRight
    }
}
