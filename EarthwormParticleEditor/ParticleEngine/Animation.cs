﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthwormParticleEditor.ParticleEngine
{
    /// <summary>
    /// This class handle all game animation such as frame counter, sprite cut, etc. and create draw position on sprite texture.
    /// </summary>
    class Animation
    {
        private Vector2 textureSize;
        private Vector2 spriteCut;
        private int currentFrame;
        private uint frameCounter;
        private List<Vector2> frames;
        private Rectangle SpriteAnimation;
        private uint frameListCounter;

        /// <summary>
        /// Create Animation class
        /// </summary>
        /// <param name="TextureSize">Size of texture.</param>
        /// <param name="SpriteCut">Size of a frame in whole spritesheet.</param>
        public Animation(Vector2 TextureSize,Vector2 SpriteCut)
        {
            textureSize = TextureSize;
            spriteCut = SpriteCut;
            frameCounter = 100;
            frameListCounter = 0;
            //split to frame
            //calculate total x,y
            int totalX = (int)(textureSize.X / spriteCut.X);
            int totalY = (int)(textureSize.Y / spriteCut.Y);
            SpriteAnimation = new Rectangle();

            frames = new List<Vector2>();

            for (int i = 0; i < totalY; i++)
                for (int j = 0; j < totalX; j++)
                    frames.Add(new Vector2(spriteCut.X * j,spriteCut.Y * i));
        }
        /// <summary>
        /// Set animation frame by number.
        /// </summary>
        /// <param name="FrameNumber">Number of frame.</param>
        /// <returns>Set result</returns>
        public bool SetFrame(int FrameNumber)
        {
            if (FrameNumber < frames.Count)
            {
                currentFrame = FrameNumber;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Animate sprite form start frame number to end frame number.
        /// This should be called in update method.
        /// </summary>
        /// <param name="FrameStart">Number of first frame.</param>
        /// <param name="FrameEnd">Number of last frame, must be more value than FrameStart.</param>
        /// <param name="AnimateSpeed">Speed of animation.(count by frame time)</param>
        public void Animate(int FrameStart, int FrameEnd, uint AnimateSpeed)
        {
            if(currentFrame < FrameStart || currentFrame > FrameEnd)
                currentFrame = FrameStart;
            if (frameCounter < AnimateSpeed)
                frameCounter++;
            else
            {
                currentFrame++;
                frameCounter = 0;
                if (currentFrame > FrameEnd)
                    currentFrame = FrameStart;
            }
        }

        /// <summary>
        /// Animate sprite form list for frames.
        /// This should be called in update method.
        /// </summary>
        /// <param name="FrameList">List contain frame numbers</param>
        /// <param name="AnimateSpeed">Speed of animation.(count by frame time)</param>
        public void Animate(List<int> FrameList, uint AnimateSpeed)
        {
            if (frameListCounter < 0 || frameListCounter > FrameList.Count)
                frameListCounter = 0;
            if (frameCounter < AnimateSpeed)
                frameCounter++;
            else
            {
                frameListCounter++;
                frameCounter = 0;
                if (frameListCounter > FrameList.Count - 1)
                    frameListCounter = 0;
            }

            currentFrame = FrameList[(int)frameListCounter];
        }

        /// <summary>
        /// Get rectangle position on spritesheet for draw method. Used by SpriteBatch.Draw(...).
        /// </summary>
        /// <returns>Rectangle position</returns>
        public Rectangle GetSpriteAnimation()
        {
            SpriteAnimation.X = (int)frames[currentFrame].X;
            SpriteAnimation.Y = (int)frames[currentFrame].Y;
            SpriteAnimation.Width = (int)spriteCut.X;
            SpriteAnimation.Height = (int)spriteCut.Y;
            return SpriteAnimation;
        }

        /// <summary>
        /// Get current frame number.
        /// </summary>
        /// <returns>Current frame number</returns>
        public int GetCurrentFreme()
        {
            return currentFrame;
        }

        /// <summary>
        /// Get total frame of the sprite texture calculate by frame size.
        /// </summary>
        /// <returns>Total frame</returns>
        public int GetTotalFrame()
        {
            return frames.Count;
        }
    }
}
