﻿using EarthwormParticleEditor.ParticleEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EarthwormParticleEditor
{
    class Button
    {
        private Vector2 Position;
        private Vector2 Size;
        public string Text;
        private Color ButtonColor;
        public Color TextColor;
        Texture2D t;
        Editor e;

        public Button(Vector2 position, Vector2 size, string text, Editor editor)
        {
            Position = position;
            Size = size;
            Text = text;
            ButtonColor = new Color(200,200,200);
            TextColor = Color.White;
            t = new Texture2D(editor.GraphicsDevice, 1, 1);
            t.SetData(new[] { ButtonColor });
            e = editor;
        }

        public bool Update()
        {
            if (MouseInput.ButtonHit(MouseButton.MouseButtonLeft))
            {
                Vector2 mousePos = MouseInput.GetCursorPosition();
                if (mousePos.X > Position.X
                    && mousePos.Y > Position.Y
                    && mousePos.X < Position.X + Size.X
                    && mousePos.Y < Position.Y + Size.Y)
                    return true;
            }
            return false;
        }

        public void Draw()
        {
            
            e.spriteBatch.Draw(t, new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y), null, ButtonColor, 0.0f, new Vector2(0.0f, 0.0f), SpriteEffects.None, 0.99f);
            e.spriteBatch.DrawString(e.Font, Text, Position + new Vector2(2, 2), TextColor,0,Vector2.Zero,1.0f,SpriteEffects.None,1.0f);
        }
    }
}
