using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using EarthwormParticleEditor.ParticleEngine;
using System.Windows.Forms;
using System;

namespace EarthwormParticleEditor
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Editor : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        EffectManager particleManager;

        PropertySelected UserSelect;
        string StringEditor;

        Vector2 textureSize;

        public SpriteFont Font;

        //menu button
        Button buttonNew;
        Button buttonLoad;
        Button buttonSave;

        //particle effect properties button
        Button bPositionX;
        Button bPositionY;
        Button bPositionZ;
        Button bMaxParticle;
        Button bGenerateTime;
        Button bGenerateTimeRange;
        Button bGravityX;
        Button bGravityY;
        Button bGravityZ;
        Button bDirX;
        Button bDirY;
        Button bDirZ;
        Button bDirRangeX;
        Button bDirRangeY;
        Button bDirRangeZ;
        Button bSpeedRange;
        Button bAngle;
        Button bAngularVelocity;
        Button bAngularVelocityRange;
        Button bColorR;
        Button bColorG;
        Button bColorB;
        Button bColorA;
        Button bMaxColorR;
        Button bMaxColorG;
        Button bMaxColorB;
        Button bMaxColorA;
        Button bSize;
        Button bMaxSize;
        Button bTTL;
        Button bTTLRange;
        Button bDepth;
        Button bRepeat;
        Button bTexturePath;
        Button bSpriteCutX;
        Button bSpriteCutY;
        Button bAnimateTime;
        Button bRandomFrame;
        Button bIsCollide;
        Button bCollideWidth;
        Button bCollideHeight;
        Button bCollidePivotX;
        Button bCollidePivotY;

        //particle properties
        private Vector3 pEmitPositionRange;
        private uint pMaxParticle;
        private float pGenerateTime;
        private float pGenerateTimeRange;
        private Vector3 pGravity;
        private Vector3 pDirection;
        private Vector3 pDirectionRange;
        private float pSpeedRange;
        private float pAngle;
        private float pAngularVelocity;
        private float pAngularVelocityRange;
        private Vector4 pColor;
        private Vector4 pMaxColor;
        private float pSize;
        private float pMaxSize;
        private float pTTL;
        private float pTTLRange;
        private float pDepth;
        private bool pRepeat;
        private Vector2 pSpriteCut;
        private string texturePath;
        private int pAnimateTime;
        private bool pRandomFrame;
        private bool pIsCollide;
        private Vector2 pCollideSize;
        private Vector2 pCollidePivot;

        OpenFileDialog opFileDialog;
        SaveFileDialog svFileDialog;
        string particleFileName;

        private int particleCount;

        public Editor()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 960;
            graphics.PreferredBackBufferHeight = 640;
            Content.RootDirectory = "Content";

            ResolutionManager.Init(ref graphics);

            this.IsMouseVisible = true;
            Mouse.WindowHandle = Window.Handle;

            particleManager = new EffectManager(this);

            StringEditor = "";
            UserSelect = PropertySelected.None;
            InitNewParticleEffect();
            particleCount = 0;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //menu button
            buttonNew = new Button(new Vector2(0, 0), new Vector2(60, 25), "NEW", this);
            buttonLoad = new Button(new Vector2(70, 0), new Vector2(60, 25), "LOAD", this);
            buttonSave = new Button(new Vector2(140, 0), new Vector2(60, 25), "SAVE", this);

            //particle effect properties button
            bPositionX = new Button(new Vector2(5, 70), new Vector2(60, 20), pEmitPositionRange.X.ToString(), this);
            bPositionY = new Button(new Vector2(80, 70), new Vector2(60, 20), pEmitPositionRange.Y.ToString(), this);
            bPositionZ = new Button(new Vector2(155, 70), new Vector2(60, 20), pEmitPositionRange.Z.ToString(), this);
            bMaxParticle = new Button(new Vector2(895, 70), new Vector2(60, 20), pMaxParticle.ToString(), this);
            bGenerateTime = new Button(new Vector2(5, 120), new Vector2(60, 20), pGenerateTime.ToString(), this);
            bGenerateTimeRange = new Button(new Vector2(5, 160), new Vector2(60, 20), pGenerateTimeRange.ToString(), this);
            bGravityX = new Button(new Vector2(5, 210), new Vector2(60, 20), pGravity.X.ToString(), this);
            bGravityY = new Button(new Vector2(80, 210), new Vector2(60, 20), pGravity.Y.ToString(), this);
            bGravityZ = new Button(new Vector2(155, 210), new Vector2(60, 20), pGravity.Z.ToString(), this);
            bDirX = new Button(new Vector2(5, 260), new Vector2(60, 20), pDirection.X.ToString(), this);
            bDirY = new Button(new Vector2(80, 260), new Vector2(60, 20), pDirection.Y.ToString(), this);
            bDirZ = new Button(new Vector2(155, 260), new Vector2(60, 20), pDirection.Z.ToString(), this);
            bDirRangeX = new Button(new Vector2(5, 300), new Vector2(60, 20), pDirectionRange.X.ToString(), this);
            bDirRangeY = new Button(new Vector2(80, 300), new Vector2(60, 20), pDirectionRange.Y.ToString(), this);
            bDirRangeZ = new Button(new Vector2(155, 300), new Vector2(60, 20), pDirectionRange.Z.ToString(), this);
            bSpeedRange = new Button(new Vector2(5, 340), new Vector2(60, 20), pSpeedRange.ToString(), this);
            bAngle = new Button(new Vector2(5, 390), new Vector2(60, 20), pAngle.ToString(), this);
            bAngularVelocity = new Button(new Vector2(5, 430), new Vector2(60, 20), pAngularVelocity.ToString(), this);
            bAngularVelocityRange = new Button(new Vector2(5, 470), new Vector2(60, 20), pAngularVelocityRange.ToString(), this);
            bColorR = new Button(new Vector2(5, 520), new Vector2(40, 20), pColor.X.ToString(), this);
            bColorG = new Button(new Vector2(60, 520), new Vector2(40, 20), pColor.Y.ToString(), this);
            bColorB = new Button(new Vector2(115, 520), new Vector2(40, 20), pColor.Z.ToString(), this);
            bColorA = new Button(new Vector2(170, 520), new Vector2(40, 20), pColor.W.ToString(), this);
            bMaxColorR = new Button(new Vector2(5, 560), new Vector2(40, 20), pMaxColor.X.ToString(), this);
            bMaxColorG = new Button(new Vector2(60, 560), new Vector2(40, 20), pMaxColor.Y.ToString(), this);
            bMaxColorB = new Button(new Vector2(115, 560), new Vector2(40, 20), pMaxColor.Z.ToString(), this);
            bMaxColorA = new Button(new Vector2(170, 560), new Vector2(40, 20), pMaxColor.W.ToString(), this);
            bSize = new Button(new Vector2(870, 310), new Vector2(40, 20), pSize.ToString(), this);
            bMaxSize = new Button(new Vector2(915, 310), new Vector2(40, 20), pMaxSize.ToString(), this);
            bTTL = new Button(new Vector2(895, 120), new Vector2(60, 20), pTTL.ToString(), this);
            bTTLRange = new Button(new Vector2(895, 160), new Vector2(60, 20), pTTLRange.ToString(), this);
            bDepth = new Button(new Vector2(895, 210), new Vector2(60, 20), pDepth.ToString(), this);
            bRepeat = new Button(new Vector2(895, 260), new Vector2(60, 20), pRepeat.ToString(), this);
            bTexturePath = new Button(new Vector2(755, 615), new Vector2(200, 20), particleFileName, this);
            bSpriteCutX = new Button(new Vector2(870, 360), new Vector2(40, 20), pSpriteCut.X.ToString(), this);
            bSpriteCutY = new Button(new Vector2(915, 360), new Vector2(40, 20), pSpriteCut.Y.ToString(), this);
            bAnimateTime = new Button(new Vector2(895, 400), new Vector2(60, 20), pAnimateTime.ToString(), this);
            bRandomFrame = new Button(new Vector2(895, 440), new Vector2(60, 20), pRandomFrame.ToString(), this);
            bIsCollide = new Button(new Vector2(895, 490), new Vector2(60, 20), pIsCollide.ToString(), this);
            bCollideWidth = new Button(new Vector2(870, 530), new Vector2(40, 20), pCollideSize.X.ToString(), this);
            bCollideHeight = new Button(new Vector2(915, 530), new Vector2(40, 20), pCollideSize.Y.ToString(), this);
            bCollidePivotX = new Button(new Vector2(870, 570), new Vector2(40, 20), pCollideSize.X.ToString(), this);
            bCollidePivotY = new Button(new Vector2(915, 570), new Vector2(40, 20), pCollideSize.Y.ToString(), this);

            // TODO: Add your initialization logic here
            particleManager.Init();
            Calculator.Init(this);
            //create interface

            //create particle
            ParticleEffect par = new ParticleEffect(this.GraphicsDevice,this.particleManager , new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);

            particleManager.Add(par);

            opFileDialog = new OpenFileDialog();
            svFileDialog = new SaveFileDialog();

            MouseInput.Update();
            ParticleEngine.KeyboardInput.Update();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Font = Content.Load<SpriteFont>("resources/font/gameFont");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == Microsoft.Xna.Framework.Input.ButtonState.Pressed)
                this.Exit();

            //count particle
            if (particleManager.ParticleEffectArray.Count > 0)
                particleCount = particleManager.ParticleEffectArray[0].GetParticleAliveCount();
            else
                particleCount = 0;

            if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.Enter) || MouseInput.ButtonHit(MouseButton.MouseButtonLeft))
            {
                if (StringEditor.Length == 0)
                    StringEditor = "0";
                switch (UserSelect)
                {
                    case PropertySelected.EmitPositionRangeX:
                        pEmitPositionRange.X = Math.Abs(float.Parse(StringEditor));
                        bPositionX.Text = pEmitPositionRange.X.ToString();
                        break;
                    case PropertySelected.EmitPositionRangeY:
                        pEmitPositionRange.Y = Math.Abs(float.Parse(StringEditor));
                        bPositionY.Text = pEmitPositionRange.Y.ToString();
                        break;
                    case PropertySelected.EmitPositionRangeZ:
                        pEmitPositionRange.Z = Math.Abs(float.Parse(StringEditor));
                        bPositionZ.Text = pEmitPositionRange.Z.ToString();
                        break;
                    case PropertySelected.MaxParticle:
                        if (uint.Parse(StringEditor) < 0) pMaxParticle = 0;
                        else pMaxParticle = uint.Parse(StringEditor);
                        bMaxParticle.Text = pMaxParticle.ToString();
                        break;
                    case PropertySelected.GenerateTime:
                        pGenerateTime = float.Parse(StringEditor);
                        if (pGenerateTime < 0) pGenerateTime = 0;
                        bGenerateTime.Text = pGenerateTime.ToString();
                        break;
                    case PropertySelected.GenerateTimeRange:
                        pGenerateTimeRange = Math.Abs(float.Parse(StringEditor));
                        bGenerateTimeRange.Text = pGenerateTimeRange.ToString();
                        break;
                    case PropertySelected.GravityX:
                        pGravity.X = float.Parse(StringEditor);
                        bGravityX.Text = pGravity.X.ToString();
                        break;
                    case PropertySelected.GravityY:
                        pGravity.Y = float.Parse(StringEditor);
                        bGravityY.Text = pGravity.Y.ToString();
                        break;
                    case PropertySelected.GravityZ:
                        pGravity.Z = float.Parse(StringEditor);
                        bGravityZ.Text = pGravity.Z.ToString();
                        break;
                    case PropertySelected.DirectionX:
                        pDirection.X = float.Parse(StringEditor);
                        bDirX.Text = pDirection.X.ToString();
                        break;
                    case PropertySelected.DirectionY:
                        pDirection.Y = float.Parse(StringEditor);
                        bDirY.Text = pDirection.Y.ToString();
                        break;
                    case PropertySelected.DirectionZ:
                        pDirection.Z = float.Parse(StringEditor);
                        bDirZ.Text = pDirection.Z.ToString();
                        break;
                    case PropertySelected.DirectionRangeX:
                        pDirectionRange.X = float.Parse(StringEditor);
                        bDirRangeX.Text = pDirectionRange.X.ToString();
                        break;
                    case PropertySelected.DirectionRangeY:
                        pDirectionRange.Y = float.Parse(StringEditor);
                        bDirRangeY.Text = pDirectionRange.Y.ToString();
                        break;
                    case PropertySelected.DirectionRangeZ:
                        pDirectionRange.Z = float.Parse(StringEditor);
                        bDirRangeZ.Text = pDirectionRange.Z.ToString();
                        break;
                    case PropertySelected.SpeedRange:
                        pSpeedRange = Math.Abs(float.Parse(StringEditor));
                        bSpeedRange.Text = pSpeedRange.ToString();
                        break;
                    case PropertySelected.Angle:
                        pAngle = float.Parse(StringEditor);
                        bAngle.Text = pAngle.ToString();
                        break;
                    case PropertySelected.AngularVelocity:
                        pAngularVelocity = float.Parse(StringEditor);
                        bAngularVelocity.Text = pAngularVelocity.ToString();
                        break;
                    case PropertySelected.AngularVelocityRange:
                        pAngularVelocityRange = float.Parse(StringEditor);
                        bAngularVelocityRange.Text = pAngularVelocityRange.ToString();
                        break;
                    case PropertySelected.ColorR:
                        pColor.X = float.Parse(StringEditor);
                        if (pColor.X < 0) pColor.X = 0;
                        else if (pColor.X > 255) pColor.X = 255;
                        bColorR.Text = pColor.X.ToString();
                        break;
                    case PropertySelected.ColorG:
                        pColor.Y = float.Parse(StringEditor);
                        if (pColor.Y < 0) pColor.Y = 0;
                        else if (pColor.Y > 255) pColor.Y = 255;
                        bColorG.Text = pColor.Y.ToString();
                        break;
                    case PropertySelected.ColorB:
                        pColor.Z = float.Parse(StringEditor);
                        if (pColor.Z < 0) pColor.Z = 0;
                        else if (pColor.Z > 255) pColor.Z = 255;
                        bColorB.Text = pColor.Z.ToString();
                        break;
                    case PropertySelected.ColorA:
                        pColor.W = float.Parse(StringEditor);
                        if (pColor.W < 0) pColor.W = 0;
                        else if (pColor.W > 255) pColor.W = 255;
                        bColorA.Text = pColor.W.ToString();
                        break;
                    case PropertySelected.MaxColorR:
                        pMaxColor.X = float.Parse(StringEditor);
                        if (pMaxColor.X < 0) pMaxColor.X = 0;
                        else if (pMaxColor.X > 255) pMaxColor.X = 255;
                        bMaxColorR.Text = pMaxColor.X.ToString();
                        break;
                    case PropertySelected.MaxColorG:
                        pMaxColor.Y = float.Parse(StringEditor);
                        if (pMaxColor.Y < 0) pMaxColor.Y = 0;
                        else if (pMaxColor.Y > 255) pMaxColor.Y = 255;
                        bMaxColorG.Text = pMaxColor.Y.ToString();
                        break;
                    case PropertySelected.MaxColorB:
                        pMaxColor.Z = float.Parse(StringEditor);
                        if (pMaxColor.Z < 0) pMaxColor.Z = 0;
                        else if (pMaxColor.Z > 255) pMaxColor.Z = 255;
                        bMaxColorB.Text = pMaxColor.Z.ToString();
                        break;
                    case PropertySelected.MaxColorA:
                        pMaxColor.W = float.Parse(StringEditor);
                        if (pMaxColor.W < 0) pMaxColor.W = 0;
                        else if (pMaxColor.W > 255) pMaxColor.W = 255;
                        bMaxColorA.Text = pMaxColor.W.ToString();
                        break;
                    case PropertySelected.StartSize:
                        pSize = float.Parse(StringEditor);
                        bSize.Text = pSize.ToString();
                        break;
                    case PropertySelected.MaxSize:
                        pMaxSize = float.Parse(StringEditor);
                        bMaxSize.Text = pMaxSize.ToString();
                        break;
                    case PropertySelected.TTL:
                        if (float.Parse(StringEditor) < 0) pTTL = 0;
                        else pTTL = float.Parse(StringEditor);
                        bTTL.Text = pTTL.ToString();
                        break;
                    case PropertySelected.TTLRange:
                        if (float.Parse(StringEditor) < 0) pTTLRange = 0;
                        else pTTLRange = float.Parse(StringEditor);
                        bTTLRange.Text = pTTLRange.ToString();
                        break;
                    case PropertySelected.Depth:
                        if (float.Parse(StringEditor) < 0) pDepth = 0;
                        else if (float.Parse(StringEditor) > 1) pDepth = 1;
                        else pDepth = float.Parse(StringEditor);
                        bDepth.Text = pDepth.ToString();
                        break;
                    case PropertySelected.SpriteCutX:
                        pSpriteCut.X = float.Parse(StringEditor);
                        if (pSpriteCut.X > textureSize.X) pSpriteCut.X = textureSize.X;
                        if (pSpriteCut.X < 1) pSpriteCut.X = 1;
                        bSpriteCutX.Text = pSpriteCut.X.ToString();
                        break;
                    case PropertySelected.SpriteCutY:
                        pSpriteCut.Y = float.Parse(StringEditor);
                        if (pSpriteCut.Y > textureSize.Y) pSpriteCut.Y = textureSize.Y;
                        if (pSpriteCut.Y < 1) pSpriteCut.Y = 1;
                        bSpriteCutY.Text = pSpriteCut.Y.ToString();
                        break;
                    case PropertySelected.AnimateTime:
                        pAnimateTime = int.Parse(StringEditor);
                        if (pAnimateTime < 0) pAnimateTime = 0;
                        bAnimateTime.Text = pAnimateTime.ToString();
                        break;
                    case PropertySelected.CollisionBoxX:
                        pCollideSize.X = int.Parse(StringEditor);
                        if (pCollideSize.X < 0) pCollideSize.X = 0;
                        bCollideWidth.Text = pCollideSize.X.ToString();
                        break;
                    case PropertySelected.CollisionBoxY:
                        pCollideSize.Y = int.Parse(StringEditor);
                        if (pCollideSize.Y < 0) pCollideSize.Y = 0;
                        bCollideHeight.Text = pCollideSize.Y.ToString();
                        break;
                    case PropertySelected.CollisionPivotX:
                        pCollidePivot.X = int.Parse(StringEditor);
                        bCollidePivotX.Text = pCollidePivot.X.ToString();
                        break;
                    case PropertySelected.CollisionPivotY:
                        pCollidePivot.Y = int.Parse(StringEditor);
                        bCollidePivotY.Text = pCollidePivot.Y.ToString();
                        break;
                }

                if (UserSelect != PropertySelected.None)
                {
                    //recreate particle effect
                    ParticleEffect par = new ParticleEffect(this.GraphicsDevice, this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                        this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                        pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                    particleManager.RemoveAll();
                    particleManager.Add(par);
                }
                
                UserSelect = PropertySelected.None;
            }

            //menu button
            if (buttonNew.Update())
            {
                this.InitNewParticleEffect();

                //reset button
                ResetButton();

                //recreate particle effect
                ParticleEffect par = new ParticleEffect(this.GraphicsDevice, this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                    this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity,pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                    pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                particleManager.RemoveAll();
                particleManager.Add(par);
            }
            if (buttonLoad.Update())
            {
                opFileDialog.Title = "Load particle template file";
                svFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory() + @"/Content/resources/effects/";
                opFileDialog.Filter = "PART files (*.part)|*.part";
                opFileDialog.Multiselect = false;
                if (opFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader sr = new StreamReader(opFileDialog.FileName);
                    string[] load = sr.ReadLine().Split('=');
                    sr.Close();

                    string emitpos = load[0];
                    pMaxParticle = uint.Parse(load[1]);
                    pGenerateTime = float.Parse(load[2]);
                    pGenerateTimeRange = float.Parse(load[3]);
                    string gra = load[4];
                    string dir = load[5];
                    string dirRange = load[6];
                    pSpeedRange = float.Parse(load[7]);
                    pAngle = float.Parse(load[8]);
                    pAngularVelocity = float.Parse(load[9]);
                    pAngularVelocityRange = float.Parse(load[10]);
                    string sColor = load[11];
                    string fColor = load[12];
                    pSize = float.Parse(load[13]);
                    pMaxSize = float.Parse(load[14]);
                    pTTL = float.Parse(load[15]);
                    pTTLRange = float.Parse(load[16]);
                    pDepth = float.Parse(load[17]);
                    pRepeat = bool.Parse(load[18]);
                    string sCut = load[19];
                    pAnimateTime = int.Parse(load[20]);
                    texturePath = load[21];
                    particleFileName = load[22];
                    pRandomFrame = bool.Parse(load[23]); 
                    //for newer version, load collision box data
                    if(load.Length == 27)
                    {
                        pIsCollide = bool.Parse(load[24]);
                        string[] cSize = load[25].Split(',');
                        pCollideSize = new Vector2(int.Parse(cSize[0]), int.Parse(cSize[1]));
                        cSize = load[26].Split(',');
                        pCollidePivot = new Vector2(int.Parse(cSize[0]), int.Parse(cSize[1]));
                    }

                    load = emitpos.Split(',');
                    pEmitPositionRange.X = float.Parse(load[0]);
                    pEmitPositionRange.Y = float.Parse(load[1]);
                    pEmitPositionRange.Z = float.Parse(load[2]);
                    load = gra.Split(',');
                    pGravity.X = float.Parse(load[0]);
                    pGravity.Y = float.Parse(load[1]);
                    pGravity.Z = float.Parse(load[2]);
                    load = dir.Split(',');
                    pDirection.X = float.Parse(load[0]);
                    pDirection.Y = float.Parse(load[1]);
                    pDirection.Z = float.Parse(load[2]);
                    load = dirRange.Split(',');
                    pDirectionRange.X = float.Parse(load[0]);
                    pDirectionRange.Y = float.Parse(load[1]);
                    pDirectionRange.Z = float.Parse(load[2]);
                    load = sColor.Split(',');
                    pColor = new Vector4(float.Parse(load[0]),float.Parse(load[1]),float.Parse(load[2]),float.Parse(load[3]));
                    load = fColor.Split(',');
                    pMaxColor = new Vector4(float.Parse(load[0]),float.Parse(load[1]),float.Parse(load[2]),float.Parse(load[3]));
                    load = sCut.Split(',');
                    pSpriteCut.X = float.Parse(load[0]);
                    pSpriteCut.Y = float.Parse(load[1]);

                    ResetButton();

                    //recreate particle effect
                    ParticleEffect par = new ParticleEffect(this.GraphicsDevice, this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                        this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                        pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                    particleManager.RemoveAll();
                    particleManager.Add(par);
                }
            }
            else if (buttonSave.Update())
            {
                svFileDialog.Title = "Save particle template file";
                svFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory() + @"/Content/resources/effects/";
                svFileDialog.Filter = "PART files (*.part)|*.part";
                if (svFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string save = "";
                    save = save + pEmitPositionRange.X.ToString() + "," + pEmitPositionRange.Y.ToString() + "," + pEmitPositionRange.Z.ToString() + "=";
                    save = save + pMaxParticle.ToString() + "=";
                    save = save + pGenerateTime.ToString() + "=";
                    save = save + pGenerateTimeRange.ToString() + "=";
                    save = save + pGravity.X.ToString() + "," + pGravity.Y.ToString() +"," + pGravity.Z.ToString() + "=";
                    save = save + pDirection.X.ToString() +","+ pDirection.Y.ToString() + "," + pDirection.Z.ToString() + "=";
                    save = save + pDirectionRange.X.ToString() + "," + pDirectionRange.Y.ToString() + "," + pDirectionRange.Z.ToString() + "=";
                    save = save + pSpeedRange.ToString() + "=";
                    save = save + pAngle.ToString() + "=";
                    save = save + pAngularVelocity.ToString() + "=";
                    save = save + pAngularVelocityRange.ToString() + "=";
                    save = save + pColor.X.ToString() + "," + pColor.Y.ToString() + "," + pColor.Z.ToString() + "," + pColor.W.ToString() + "=";
                    save = save + pMaxColor.X.ToString() + "," + pMaxColor.Y.ToString() + "," + pMaxColor.Z.ToString() + "," + pMaxColor.W.ToString() + "=";
                    save = save + pSize.ToString() + "=";
                    save = save + pMaxSize.ToString() + "=";
                    save = save + pTTL.ToString() + "=";
                    save = save + pTTLRange.ToString() + "=";
                    save = save + pDepth.ToString() + "=";
                    save = save + pRepeat.ToString() + "=";
                    save = save + pSpriteCut.X.ToString() + "," + pSpriteCut.Y.ToString() + "=";
                    save = save + pAnimateTime.ToString() + "=";
                    save = save + texturePath + "=";
                    save = save + particleFileName + "=";
                    save = save + pRandomFrame.ToString() + "=";
                    save = save + pIsCollide.ToString() + "=";
                    save = save + pCollideSize.X.ToString() + "," + pCollideSize.Y.ToString() + "=";
                    save = save + pCollidePivot.X.ToString() + "," + pCollidePivot.Y.ToString();

                    FileStream fs = new FileStream(svFileDialog.FileName, FileMode.Create);  
                    StreamWriter sw = new StreamWriter(fs);  
 
                    sw.WriteLine(save);  
 
                    sw.Close();
                    fs.Close();
                }
            }
            //emitpos
            else if (bPositionX.Update())
            {
                UserSelect = PropertySelected.EmitPositionRangeX;
                StringEditor = pEmitPositionRange.X.ToString();
            }
            else if (bPositionY.Update())
            {
                UserSelect = PropertySelected.EmitPositionRangeY;
                StringEditor = pEmitPositionRange.Y.ToString();
            }
            else if (bPositionZ.Update())
            {
                UserSelect = PropertySelected.EmitPositionRangeZ;
                StringEditor = pEmitPositionRange.Z.ToString();
            }
            //maxparticle
            else if (bMaxParticle.Update())
            {
                UserSelect = PropertySelected.MaxParticle;
                StringEditor = pMaxParticle.ToString();
            }
            //generateTime
            else if (bGenerateTime.Update())
            {
                UserSelect = PropertySelected.GenerateTime;
                StringEditor = pGenerateTime.ToString();
            }
            //generateTimeRange
            else if (bGenerateTimeRange.Update())
            {
                UserSelect = PropertySelected.GenerateTimeRange;
                StringEditor = pGenerateTimeRange.ToString();
            }
            //gravity
            else if (bGravityX.Update())
            {
                UserSelect = PropertySelected.GravityX;
                StringEditor = pGravity.X.ToString();
            }
            else if (bGravityY.Update())
            {
                UserSelect = PropertySelected.GravityY;
                StringEditor = pGravity.Y.ToString();
            }
            else if (bGravityZ.Update())
            {
                UserSelect = PropertySelected.GravityZ;
                StringEditor = pGravity.Z.ToString();
            }
            //direction
            else if (bDirX.Update())
            {
                UserSelect = PropertySelected.DirectionX;
                StringEditor = pDirection.X.ToString();
            }
            else if (bDirY.Update())
            {
                UserSelect = PropertySelected.DirectionY;
                StringEditor = pDirection.Y.ToString();
            }
            else if (bDirZ.Update())
            {
                UserSelect = PropertySelected.DirectionZ;
                StringEditor = pDirection.Z.ToString();
            }
            //direction range
            else if (bDirRangeX.Update())
            {
                UserSelect = PropertySelected.DirectionRangeX;
                StringEditor = pDirectionRange.X.ToString();
            }
            else if (bDirRangeY.Update())
            {
                UserSelect = PropertySelected.DirectionRangeY;
                StringEditor = pDirectionRange.Y.ToString();
            }
            else if (bDirRangeZ.Update())
            {
                UserSelect = PropertySelected.DirectionRangeZ;
                StringEditor = pDirectionRange.Z.ToString();
            }
            //speed range
            else if (bSpeedRange.Update())
            {
                UserSelect = PropertySelected.SpeedRange;
                StringEditor = pSpeedRange.ToString();
            }
            //angle
            else if (bAngle.Update())
            {
                UserSelect = PropertySelected.Angle;
                StringEditor = pAngle.ToString();
            }
            else if (bAngularVelocity.Update())
            {
                UserSelect = PropertySelected.AngularVelocity;
                StringEditor = pAngularVelocity.ToString();
            }
            else if (bAngularVelocityRange.Update())
            {
                UserSelect = PropertySelected.AngularVelocityRange;
                StringEditor = pAngularVelocityRange.ToString();
            }
            //start color
            else if (bColorR.Update())
            {
                UserSelect = PropertySelected.ColorR;
                StringEditor = pColor.X.ToString();
            }
            else if (bColorG.Update())
            {
                UserSelect = PropertySelected.ColorG;
                StringEditor = pColor.Y.ToString();
            }
            else if (bColorB.Update())
            {
                UserSelect = PropertySelected.ColorB;
                StringEditor = pColor.Z.ToString();
            }
            else if (bColorA.Update())
            {
                UserSelect = PropertySelected.ColorA;
                StringEditor = pColor.W.ToString();
            }
            //final color
            else if (bMaxColorR.Update())
            {
                UserSelect = PropertySelected.MaxColorR;
                StringEditor = pMaxColor.X.ToString();
            }
            else if (bMaxColorG.Update())
            {
                UserSelect = PropertySelected.MaxColorG;
                StringEditor = pMaxColor.Y.ToString();
            }
            else if (bMaxColorB.Update())
            {
                UserSelect = PropertySelected.MaxColorB;
                StringEditor = pMaxColor.Z.ToString();
            }
            else if (bMaxColorA.Update())
            {
                UserSelect = PropertySelected.MaxColorA;
                StringEditor = pMaxColor.W.ToString();
            }
            //size
            else if (bSize.Update())
            {
                UserSelect = PropertySelected.StartSize;
                StringEditor = pSize.ToString();
            }
            else if (bMaxSize.Update())
            {
                UserSelect = PropertySelected.MaxSize;
                StringEditor = pMaxSize.ToString();
            }
            //time to live
            else if (bTTL.Update())
            {
                UserSelect = PropertySelected.TTL;
                StringEditor = pTTL.ToString();
            }
            //time to live range
            else if (bTTLRange.Update())
            {
                UserSelect = PropertySelected.TTLRange;
                StringEditor = pTTLRange.ToString();
            }
            //depth
            else if (bDepth.Update())
            {
                UserSelect = PropertySelected.Depth;
                StringEditor = pDepth.ToString();
            }
            //repeat
            else if (bRepeat.Update())
            {
                pRepeat = !pRepeat;
                bRepeat.Text = pRepeat.ToString();

                //recreate particle effect
                ParticleEffect par = new ParticleEffect(this.GraphicsDevice,this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                    this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                    pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                particleManager.RemoveAll();
                particleManager.Add(par);
            }
            //texture
            else if (bTexturePath.Update())
            {
                //open path
                opFileDialog.Title = "Browse texture file";
                opFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory()+@"\Content\resources\particles";
                opFileDialog.Multiselect = false;
                opFileDialog.Filter = "PNG files (*.png)|*.png|JPG files (*.jpg)|*.jpg|JPEG files (*.jpeg)|*.jpeg";
                if (opFileDialog.ShowDialog() == DialogResult.OK)
                {
                    texturePath = opFileDialog.FileName;
                    particleFileName = Path.GetFileNameWithoutExtension(opFileDialog.FileName);

                    //get texture size
                    using (FileStream titleStream = new FileStream(texturePath, FileMode.Open))
                    {
                        Texture2D t = Texture2D.FromStream(GraphicsDevice, titleStream);
                        titleStream.Close();
                        textureSize.X = t.Width;
                        textureSize.Y = t.Height;
                        t.Dispose();
                    }

                    pSpriteCut = textureSize;
                    bSpriteCutX.Text = pSpriteCut.X.ToString();
                    bSpriteCutY.Text = pSpriteCut.Y.ToString();

                    //recreate particle effect
                    ParticleEffect par = new ParticleEffect(this.GraphicsDevice,this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                        this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                        pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                    particleManager.RemoveAll();
                    particleManager.Add(par);

                    bTexturePath.Text = particleFileName;
                }
            }
            //sprite cut
            else if (bSpriteCutX.Update())
            {
                UserSelect = PropertySelected.SpriteCutX;
                StringEditor = pSpriteCut.X.ToString();
            }
            else if (bSpriteCutY.Update())
            {
                UserSelect = PropertySelected.SpriteCutY;
                StringEditor = pSpriteCut.Y.ToString();
            }
            else if (bAnimateTime.Update())
            {
                UserSelect = PropertySelected.AnimateTime;
                StringEditor = pAnimateTime.ToString();
            }
            //random frame
            else if (bRandomFrame.Update())
            {
                pRandomFrame = !pRandomFrame;
                bRandomFrame.Text = pRandomFrame.ToString();

                //recreate particle effect
                ParticleEffect par = new ParticleEffect(this.GraphicsDevice, this.particleManager, new Vector3(480, 320, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                    this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                    pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                particleManager.RemoveAll();
                particleManager.Add(par);
            }
            //Has collision box
            else if (bIsCollide.Update())
            {
                pIsCollide = !pIsCollide;
                bIsCollide.Text = pIsCollide.ToString();
            }
            //Collision box size
            else if(bCollideWidth.Update())
            {
                UserSelect = PropertySelected.CollisionBoxX;
                StringEditor = pCollideSize.X.ToString();
            }
            else if (bCollideHeight.Update())
            {
                UserSelect = PropertySelected.CollisionBoxY;
                StringEditor = pCollideSize.Y.ToString();
            }
            //Collision pivot
            else if (bCollidePivotX.Update())
            {
                UserSelect = PropertySelected.CollisionPivotX;
                StringEditor = pCollidePivot.X.ToString();
            }
            else if (bCollidePivotY.Update())
            {
                UserSelect = PropertySelected.CollisionPivotY;
                StringEditor = pCollidePivot.Y.ToString();
            }
            else if(MouseInput.ButtonDown(MouseButton.MouseButtonRight))
            {
                if (particleManager.ParticleEffectArray.Count > 0)
                {
                    particleManager.ParticleEffectArray[0].EmitPosition = new Vector3(MouseInput.GetCursorPosition().X, MouseInput.GetCursorPosition().Y, 0);
                }
                else
                {
                    //recreate particle effect
                    ParticleEffect par = new ParticleEffect(this.GraphicsDevice,this.particleManager, new Vector3(MouseInput.GetCursorPosition().X, MouseInput.GetCursorPosition().Y, 0), pEmitPositionRange, pMaxParticle, pGenerateTime, pGenerateTimeRange, texturePath,
                        this.Content, pGravity, pDirection, pDirectionRange, pSpeedRange, pAngle, pAngularVelocity, pAngularVelocityRange, pColor, pMaxColor, pSize, pMaxSize,
                        pTTL, pTTLRange, pDepth, pRepeat, pSpriteCut, pAnimateTime, pRandomFrame);
                    particleManager.RemoveAll();
                    particleManager.Add(par);
                }
            }

            //if selected
            if (UserSelect != PropertySelected.None)
            {
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D0) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad0))
                    StringEditor += "0";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D1) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad1))
                    StringEditor += "1";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D2) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad2))
                    StringEditor += "2";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D3) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad3))
                    StringEditor += "3";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D4) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad4))
                    StringEditor += "4";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D5) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad5))
                    StringEditor += "5";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D6) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad6))
                    StringEditor += "6";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D7) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad7))
                    StringEditor += "7";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D8) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad8))
                    StringEditor += "8";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.D9) || ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.NumPad9))
                    StringEditor += "9";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.OemPeriod))
                    StringEditor += ".";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.OemMinus))
                    StringEditor += "-";
                if (ParticleEngine.KeyboardInput.KeyHit(Microsoft.Xna.Framework.Input.Keys.Back) && StringEditor.Length > 0)
                    StringEditor = StringEditor.Remove(StringEditor.Length - 1);

                switch (UserSelect)
                {
                    case PropertySelected.EmitPositionRangeX :
                        bPositionX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.EmitPositionRangeY :
                        bPositionY.Text = StringEditor + "_";
                        break;
                    case PropertySelected.EmitPositionRangeZ:
                        bPositionZ.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxParticle:
                        bMaxParticle.Text = StringEditor + "_";
                        break;
                    case PropertySelected.GenerateTime:
                        bGenerateTime.Text = StringEditor + "_";
                        break;
                    case PropertySelected.GenerateTimeRange:
                        bGenerateTimeRange.Text = StringEditor + "_";
                        break;
                    case PropertySelected.GravityX:
                        bGravityX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.GravityY:
                        bGravityY.Text = StringEditor + "_";
                        break;
                    case PropertySelected.GravityZ:
                        bGravityZ.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionX:
                        bDirX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionY:
                        bDirY.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionZ:
                        bDirZ.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionRangeX:
                        bDirRangeX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionRangeY:
                        bDirRangeY.Text = StringEditor + "_";
                        break;
                    case PropertySelected.DirectionRangeZ:
                        bDirRangeZ.Text = StringEditor + "_";
                        break;
                    case PropertySelected.SpeedRange:
                        bSpeedRange.Text = StringEditor + "_";
                        break;
                    case PropertySelected.Angle:
                        bAngle.Text = StringEditor + "_";
                        break;
                    case PropertySelected.AngularVelocity:
                        bAngularVelocity.Text = StringEditor + "_";
                        break;
                    case PropertySelected.AngularVelocityRange:
                        bAngularVelocityRange.Text = StringEditor + "_";
                        break;
                    case PropertySelected.ColorR:
                        bColorR.Text = StringEditor + "_";
                        break;
                    case PropertySelected.ColorG:
                        bColorG.Text = StringEditor + "_";
                        break;
                    case PropertySelected.ColorB:
                        bColorB.Text = StringEditor + "_";
                        break;
                    case PropertySelected.ColorA:
                        bColorA.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxColorR:
                        bMaxColorR.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxColorG:
                        bMaxColorG.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxColorB:
                        bMaxColorB.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxColorA:
                        bMaxColorA.Text = StringEditor + "_";
                        break;
                    case PropertySelected.StartSize:
                        bSize.Text = StringEditor + "_";
                        break;
                    case PropertySelected.MaxSize:
                        bMaxSize.Text = StringEditor + "_";
                        break;
                    case PropertySelected.TTL:
                        bTTL.Text = StringEditor + "_";
                        break;
                    case PropertySelected.TTLRange:
                        bTTLRange.Text = StringEditor + "_";
                        break;
                    case PropertySelected.Depth:
                        bDepth.Text = StringEditor + "_";
                        break;
                    case PropertySelected.SpriteCutX:
                        bSpriteCutX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.SpriteCutY:
                        bSpriteCutY.Text = StringEditor + "_";
                        break;
                    case PropertySelected.AnimateTime:
                        bAnimateTime.Text = StringEditor + "_";
                        break;
                    case PropertySelected.CollisionBoxX:
                        bCollideWidth.Text = StringEditor + "_";
                        break;
                    case PropertySelected.CollisionBoxY:
                        bCollideHeight.Text = StringEditor + "_";
                        break;
                    case PropertySelected.CollisionPivotX:
                        bCollidePivotX.Text = StringEditor + "_";
                        break;
                    case PropertySelected.CollisionPivotY:
                        bCollidePivotY.Text = StringEditor + "_";
                        break;
                }
            }

            // TODO: Add your update logic here
            Calculator.Update(gameTime);
            particleManager.Update();
            MouseInput.Update();
            ParticleEngine.KeyboardInput.Update();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin(SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                null);
            //draw menu button
            buttonNew.Draw();
            buttonLoad.Draw();
            buttonSave.Draw();

            //draw particle properties
            spriteBatch.DrawString(Font, "Emit Position Range :", new Vector2(5, 50), Color.White);
            bPositionX.Draw();
            bPositionY.Draw();
            bPositionZ.Draw();
            spriteBatch.DrawString(Font, "Max Particle :", new Vector2(840, 50), Color.White);
            bMaxParticle.Draw();
            spriteBatch.DrawString(Font, "Generate Time(Secounds) :", new Vector2(5, 100), Color.White);
            bGenerateTime.Draw();
            spriteBatch.DrawString(Font, "Generate Time Range(Secounds) :", new Vector2(5, 140), Color.White);
            bGenerateTimeRange.Draw();
            spriteBatch.DrawString(Font, "Gravity :", new Vector2(5, 190), Color.White);
            bGravityX.Draw();
            bGravityY.Draw();
            bGravityZ.Draw();
            spriteBatch.DrawString(Font, "Direction :", new Vector2(5, 240), Color.White);
            bDirX.Draw();
            bDirY.Draw();
            bDirZ.Draw();
            spriteBatch.DrawString(Font, "Direction Angle Range :", new Vector2(5, 280), Color.White);
            bDirRangeX.Draw();
            bDirRangeY.Draw();
            bDirRangeZ.Draw();
            spriteBatch.DrawString(Font, "Speed Range :", new Vector2(5, 320), Color.White);
            bSpeedRange.Draw();
            spriteBatch.DrawString(Font, "Angle :", new Vector2(5, 370), Color.White);
            bAngle.Draw();
            spriteBatch.DrawString(Font, "Angular Velocity :", new Vector2(5, 410), Color.White);
            bAngularVelocity.Draw();
            spriteBatch.DrawString(Font, "Angular Velocity Range :", new Vector2(5, 450), Color.White);
            bAngularVelocityRange.Draw();
            spriteBatch.DrawString(Font, "Start Color(RGBA) :", new Vector2(5, 500), Color.White);
            bColorR.Draw();
            bColorG.Draw();
            bColorB.Draw();
            bColorA.Draw();
            spriteBatch.DrawString(Font, "Final Color(RGBA) :", new Vector2(5, 540), Color.White);
            bMaxColorR.Draw();
            bMaxColorG.Draw();
            bMaxColorB.Draw();
            bMaxColorA.Draw();
            spriteBatch.DrawString(Font, "Size(Min,Max) :", new Vector2(835, 290), Color.White);
            bSize.Draw();
            bMaxSize.Draw();
            spriteBatch.DrawString(Font, "Time to live(Secounds) :", new Vector2(760, 100), Color.White);
            bTTL.Draw();
            spriteBatch.DrawString(Font, "Time to live range(Secounds) :", new Vector2(712, 140), Color.White);
            bTTLRange.Draw();
            spriteBatch.DrawString(Font, "Depth(0.0-1.0) :", new Vector2(827, 190), Color.White);
            bDepth.Draw();
            spriteBatch.DrawString(Font, "Repeat :", new Vector2(840, 240), Color.White);
            bRepeat.Draw();
            spriteBatch.DrawString(Font, "Texture :", new Vector2(880, 595), Color.White);
            bTexturePath.Draw();
            spriteBatch.DrawString(Font, "Sprite Cut :", new Vector2(840, 340), Color.White);
            bSpriteCutX.Draw();
            bSpriteCutY.Draw();
            spriteBatch.DrawString(Font, "Animate Time(Frame) :", new Vector2(789, 380), Color.White);
            bAnimateTime.Draw();
            spriteBatch.DrawString(Font, "Random Frame :", new Vector2(845, 420), Color.White);
            bRandomFrame.Draw();
            spriteBatch.DrawString(Font, "Has Collision Box :", new Vector2(805, 470), Color.White);
            bIsCollide.Draw();
            spriteBatch.DrawString(Font, "Collision Box Size :", new Vector2(795, 510), Color.White);
            bCollideWidth.Draw();
            bCollideHeight.Draw();
            spriteBatch.DrawString(Font, "Collision Box Pivot :", new Vector2(793, 550), Color.White);
            bCollidePivotX.Draw();
            bCollidePivotY.Draw();


            spriteBatch.DrawString(Font, "FPS:"+((int)Calculator.AverageFPS).ToString(), new Vector2(895, 5), Color.White);
            spriteBatch.DrawString(Font, "Particle Count:" + particleCount, new Vector2(5, 620), Color.White);

            spriteBatch.End();

            //draw particle
            spriteBatch.Begin(SpriteSortMode.FrontToBack,
                BlendState.AlphaBlend,
                SamplerState.PointClamp,
                null,
                null,
                null,
                null);
            particleManager.Draw(spriteBatch);
            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        protected void InitNewParticleEffect()
        {
            pEmitPositionRange = new Vector3(0,0,0);
            pMaxParticle = 30;
            pGenerateTime = 0.05f;
            pGenerateTimeRange = 0.0f;
            pGravity = new Vector3(0, 0, 0.8f);
            pDirection = new Vector3(0, 0, -3);
            pDirectionRange = new Vector3(2,0,0);
            pSpeedRange = 2.0f;
            pAngle = 0;
            pAngularVelocity = 0;
            pAngularVelocityRange = 0;
            pColor = new Vector4(255,255,255,255);
            pMaxColor = new Vector4(255,0,0,0);
            pSize = 1.0f;
            pMaxSize = 1.5f;
            pTTL = 2.0f;
            pTTLRange = 0.0f;
            pDepth = 0.6f;
            pRepeat = true;
            texturePath = Directory.GetCurrentDirectory()+@"\Content\resources\particles\particle.png";
            pAnimateTime = 100;
            pIsCollide = false;
            pCollideSize = Vector2.Zero;
            pCollidePivot = Vector2.Zero;

            UserSelect = PropertySelected.None;
            particleFileName = "particle";
            textureSize = new Vector2(32, 32);
            pSpriteCut = textureSize;
            pRandomFrame = false;
        }

        private void ResetButton()
        {
            bPositionX.Text = pEmitPositionRange.X.ToString();
            bPositionY.Text = pEmitPositionRange.Y.ToString();
            bPositionZ.Text = pEmitPositionRange.Z.ToString();
            bMaxParticle.Text = pMaxParticle.ToString();
            bGenerateTime.Text = pGenerateTime.ToString();
            bGenerateTimeRange.Text = pGenerateTimeRange.ToString();
            bGravityX.Text = pGravity.X.ToString();
            bGravityY.Text = pGravity.Y.ToString();
            bGravityZ.Text = pGravity.Z.ToString();
            bDirX.Text = pDirection.X.ToString();
            bDirY.Text = pDirection.Y.ToString();
            bDirZ.Text = pDirection.Z.ToString();
            bDirRangeX.Text = pDirectionRange.X.ToString();
            bDirRangeY.Text = pDirectionRange.Y.ToString();
            bDirRangeZ.Text = pDirectionRange.Z.ToString();
            bSpeedRange.Text = pSpeedRange.ToString();
            bAngle.Text = pAngle.ToString();
            bAngularVelocity.Text = pAngularVelocity.ToString();
            bAngularVelocityRange.Text = pAngularVelocityRange.ToString();
            bColorR.Text = pColor.X.ToString();
            bColorG.Text = pColor.Y.ToString();
            bColorB.Text = pColor.Z.ToString();
            bColorA.Text = pColor.W.ToString();
            bMaxColorR.Text = pMaxColor.X.ToString();
            bMaxColorG.Text = pMaxColor.Y.ToString();
            bMaxColorB.Text = pMaxColor.Z.ToString();
            bMaxColorA.Text = pMaxColor.W.ToString();
            bSize.Text = pSize.ToString();
            bMaxSize.Text = pMaxSize.ToString();
            bTTL.Text = pTTL.ToString();
            bTTLRange.Text = pTTLRange.ToString();
            bDepth.Text = pDepth.ToString();
            bRepeat.Text = pRepeat.ToString();
            bSpriteCutX.Text = pSpriteCut.X.ToString();
            bSpriteCutY.Text = pSpriteCut.Y.ToString();
            bAnimateTime.Text = pAnimateTime.ToString();
            bTexturePath.Text = particleFileName;
            bIsCollide.Text = pIsCollide.ToString();
            bCollideWidth.Text = pCollideSize.X.ToString();
            bCollideHeight.Text = pCollideSize.Y.ToString();
            bCollidePivotX.Text = pCollidePivot.X.ToString();
            bCollidePivotY.Text = pCollidePivot.Y.ToString();
        }
    }
    enum PropertySelected
    {
        None,
        EmitPositionRangeX,
        EmitPositionRangeY,
        EmitPositionRangeZ,
        MaxParticle,
        GenerateTime,
        GenerateTimeRange,
        GravityX,
        GravityY,
        GravityZ,
        DirectionX,
        DirectionY,
        DirectionZ,
        DirectionRangeX,
        DirectionRangeY,
        DirectionRangeZ,
        SpeedRange,
        Angle,
        AngularVelocity,
        AngularVelocityRange,
        ColorR,
        ColorG,
        ColorB,
        ColorA,
        MaxColorR,
        MaxColorG,
        MaxColorB,
        MaxColorA,
        StartSize,
        MaxSize,
        TTL,
        TTLRange,
        Depth,
        SpriteCutX,
        SpriteCutY,
        AnimateTime,
        CollisionBoxX,
        CollisionBoxY,
        CollisionPivotX,
        CollisionPivotY
    }
}
